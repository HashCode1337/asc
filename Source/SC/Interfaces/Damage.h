// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Libraries/SC_Statics.h"
#include "Damage.generated.h"

class ASC_BulletBase;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDamage : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SC_API IDamage
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void ReceiveBulletHit(FName BulletName, FName HitBone, FVector BulletVelocity) {};
	virtual void CollectParentHitPartNames(TMap<FName, float>& OutTMap) {};
	virtual float GetHitPartArmor(FName PartName) { return 0.f; };

};

#include "HealthComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Net/UnrealNetwork.h"
#include "../SCGameModeBase.h"
#include "../Base/SC_CharacterBase.h"

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	IDamage* OwnerInterfaced = GetOwner<IDamage>();
	if (OwnerInterfaced)
	{
		OwnerInterfaced->CollectParentHitPartNames(DamageRegistrations);

		// Set each part damage to 0
		for (TPair<FName, float>& HitPart : DamageRegistrations)
		{
			HitPart.Value = 0.f;
		}
	}

	SetIsReplicatedByDefault(true);
}

void UHealthComponent::AddDamage_Implementation(float AddingDamage/*, float& DamageDealed*/)
{
	ASC_CharacterBase* CompOwner = Cast<ASC_CharacterBase>(GetOwner());

	UE_LOG(LogTemp, Warning, TEXT("HIT"));

	// Fix AI owning bug
	if (!CompOwner)
		return;

	if (Involnurable || !CompOwner) return;

	AddingDamage /= Armor;

	if (HittenSound && AddingDamage > 0)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HittenSound, CompOwner->GetActorLocation());
	}

	if (AddingDamage >= Health)
	{
		Health = 0.f;
		SetLifeState(ELifeState::Knocked);

		UCharacterMovementComponent* CharMovComp = Cast<UCharacterMovementComponent>(CompOwner->GetMovementComponent());
		if (CharMovComp)
		{
			CharMovComp->DisableMovement();
			//CompOwner->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			//CompOwner->GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			//CompOwner->GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
			//CompOwner->GetMesh()->SetSimulatePhysics(true);
		}

		OnDeath();

		Health = 0;

		// Hit OnHealthChanged to notify
		OnRep_HealthChanged();

		return;
	}

	if (AddingDamage <= 0 && Health >= MaxHealth)
	{
		Health = MaxHealth;

		// Hit OnHealthChanged to notify
		OnRep_HealthChanged();

		return;
	}

	Health -= AddingDamage;

	// Hit OnHealthChanged to notify
	OnRep_HealthChanged();
	
	//	OnHealthChanged.Broadcast(Health);
}

void UHealthComponent::AddDamageToPart_Implementation(float AddingDamage, FName PartName)
{
	float ResultDamage = AddingDamage;
	bool bValidPart = DamageRegistrations.Contains(PartName);

	// Exit if PartName hasn't been found
	if (!bValidPart)
	{
		UE_LOG(LogTemp, Error, TEXT("Part %s is invalid, can't register part damage"), *PartName.ToString());
		return;
	}

	// Declare default dmg multiplier
	float DmgMultiplier = 1.f;

	// Try to get interface with GetHitPartArmor()
	IDamage* OwnerInterfaced = GetOwner<IDamage>();
	if (OwnerInterfaced)
	{
		// Override default multiplier and recalculate dmg
		DmgMultiplier = OwnerInterfaced->GetHitPartArmor(PartName);
		ResultDamage *= DmgMultiplier;
	}

	// Try to apply damage
	float& CurrentPartHealth = DamageRegistrations[PartName];
	CurrentPartHealth += ResultDamage;

	AddDamage(ResultDamage);

}

void UHealthComponent::SetLifeState(ELifeState NewLifeState /*= ELifeState::Alive*/)
{
	LifeState = NewLifeState;
}

ELifeState UHealthComponent::GetLifeState()
{
	return LifeState;
}

float UHealthComponent::GetHealthConverted()
{
	return UKismetMathLibrary::MapRangeClamped(Health, 0.f, MaxHealth, 0.f, 1.f);
}

void UHealthComponent::OnRep_HealthChanged()
{
	OnHealthChanged.Broadcast(Health);
}

void UHealthComponent::OnDeath_Implementation()
{
	ASCGameModeBase* GM = Cast<ASCGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));

	APawn* CompOwnerAsPawn = Cast<APawn>(GetOwner());
	if (!CompOwnerAsPawn) return;
	
	APlayerController* PC = Cast<APlayerController>(CompOwnerAsPawn->GetController());
	if (!PC) return;
	
	GM->RespawnPlayer(CompOwnerAsPawn, PC);
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;
}

// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, LifeState);
	DOREPLIFETIME(UHealthComponent, Health);
}
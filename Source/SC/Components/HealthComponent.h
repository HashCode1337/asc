// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Libraries/SC_Statics.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthCompHPChanged, float, NewHealth);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class SC_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthComponent();


	// Vars
	UPROPERTY(ReplicatedUsing = OnRep_HealthChanged, EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	float Health = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	float MaxHealth = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	TMap<FName, float> DamageRegistrations;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	float Armor = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	bool Involnurable = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	USoundBase* HittenSound = nullptr;
	UPROPERTY(Replicated, VisibleDefaultsOnly, BlueprintReadOnly, Category = "Health Settings")
	ELifeState LifeState = ELifeState::Alive;

	// Funcs
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void AddDamage(float AddingDamage);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void AddDamageToPart(float AddingDamage, FName PartName);
	UFUNCTION(BlueprintCallable)
	void SetLifeState(ELifeState NewLifeState = ELifeState::Alive);
	UFUNCTION(BlueprintCallable)
	ELifeState GetLifeState();
	UFUNCTION(BlueprintCallable)
	float GetHealthConverted();
	UFUNCTION()
	void OnRep_HealthChanged();


	// Events
	
	UPROPERTY(BlueprintAssignable)
	FHealthCompHPChanged OnHealthChanged;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnDeath();
	/*UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintAssignable)
	void OnHealthChanged(float NewHealth);*/



protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};

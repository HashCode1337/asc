// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "../Libraries/SC_Statics.h"
#include "SC_RespawnPointFaction.generated.h"

/**
 * 
 */
UCLASS()
class SC_API ASC_RespawnPointFaction : public ATargetPoint
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EFaction RespawnPointFaction;


	
};

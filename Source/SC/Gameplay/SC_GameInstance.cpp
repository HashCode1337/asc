// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_GameInstance.h"

bool USC_GameInstance::GetBulletInfo(FName BulletName, FBulletDT& BulletData)
{
	bool bFound = false;
	FBulletDT* BulletExpectedData;

	if (!BulletDataTable) return false;
	
	BulletExpectedData = BulletDataTable->FindRow<FBulletDT>(BulletName, "", false);
	if (BulletExpectedData)
	{
		BulletData = *BulletExpectedData;
		bFound = true;
	}

	return bFound;
}

bool USC_GameInstance::GetWeaponInfo(FName BulletName, FWeaponDT& WeaponData)
{
	bool bFound = false;
	FWeaponDT* WeaponExpectedData;

	if (!WeaponDataTable) return false;

	WeaponExpectedData = WeaponDataTable->FindRow<FWeaponDT>(BulletName, "", false);
	if (WeaponExpectedData)
	{
		WeaponData = *WeaponExpectedData;
		bFound = true;
	}

	return bFound;
}
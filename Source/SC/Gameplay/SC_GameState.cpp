// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_GameState.h"
#include "SC_GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "../Libraries/SC_Statics.h"

void ASC_GameState::PlayBulletImpactFX_Implementation(FName BulletName, EPhysicalSurface SurfaceType, FTransform ImpactTransform, bool bPlaySound)
{
	USC_GameInstance* GI = GetWorld()->GetGameInstance<USC_GameInstance>();
	if (GI)
	{
		UDataTable* BDT = GI->BulletDataTable;
		FBulletDT BulletSettings;

		bool bIsFound = GI->GetBulletInfo(BulletName, BulletSettings);
		GI->GetBulletInfo(BulletName, BulletSettings);
		
		if (bIsFound)
		{
			if (BulletSettings.HitSounds.Contains(SurfaceType) && bPlaySound)
			{
				USoundBase* HitSound = BulletSettings.HitSounds[SurfaceType];
				if (HitSound)
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, ImpactTransform.GetLocation());
			}

			if (BulletSettings.HitParticles.Contains(SurfaceType))
			{
				UParticleSystem* HitFX = BulletSettings.HitParticles[SurfaceType];
				if (HitFX)
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, ImpactTransform);
			}
		}
	}
}

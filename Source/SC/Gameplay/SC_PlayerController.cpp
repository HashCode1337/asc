// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_PlayerController.h"
#include "../SC_HUD.h"

void ASC_PlayerController::OnPossess(APawn* NewPawn)
{
	Super::OnPossess(NewPawn);

	RequestResetHUD();
}

void ASC_PlayerController::RequestResetHUD_Implementation()
{
	GEngine->AddOnScreenDebugMessage(0, 5, FColor::Red, "MES");
	ASC_HUD* PlayerHUD = GetHUD<ASC_HUD>();
	if (PlayerHUD)
	{
		PlayerHUD->ResetHUD();
	}
	UE_LOG(LogTemp, Warning, TEXT("Posses"));
}

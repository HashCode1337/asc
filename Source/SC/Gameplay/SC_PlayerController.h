// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/ALSPlayerController.h"
#include "SC_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SC_API ASC_PlayerController : public AALSPlayerController
{
	GENERATED_BODY()
	
public:

	virtual void OnPossess(APawn* NewPawn) override;
	
	UFUNCTION(Client, Reliable)
	void RequestResetHUD();

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "SC_GameState.generated.h"

/**
 * 
 */
UCLASS()
class SC_API ASC_GameState : public AGameStateBase
{
	GENERATED_BODY()

public:

	UFUNCTION(NetMulticast, Reliable)
	void PlayBulletImpactFX(FName BulletName, EPhysicalSurface SurfaceType, FTransform ImpactTransform, bool bPlaySound);
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "../Libraries/SC_Statics.h"
#include "SC_GameInstance.generated.h"

class UDataTable;

UCLASS()
class SC_API USC_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	// DataTables
	/*------------------------------------------------------------------------------------------------------------------------*/

	UPROPERTY(EditDefaultsOnly, Category = "Bullet")
	UDataTable* BulletDataTable;

	UPROPERTY(EditDefaultsOnly, Category = "Bullet")
	UDataTable* WeaponDataTable;



	UFUNCTION(BlueprintCallable)
	bool GetBulletInfo(FName BulletName, FBulletDT& BulletData);
	
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfo(FName BulletName, FWeaponDT& WeaponData);

	// Points
	/*------------------------------------------------------------------------------------------------------------------------*/
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_CharacterBase.h"
#include "SC_WeaponBase.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Character/Animation/ALSCharacterAnimInstance.h"
#include "Library/ALSMathLibrary.h"
#include "../SC_HUD.h"
#include "../Gameplay/SC_GameInstance.h"

const FName NAME_FP_Camera(TEXT("FP_Camera"));
const FName NAME_Pelvis(TEXT("Pelvis"));
const FName NAME_RagdollPose(TEXT("RagdollPose"));
const FName NAME_RotationAmount(TEXT("RotationAmount"));
const FName NAME_YawOffset(TEXT("YawOffset"));
const FName NAME_pelvis(TEXT("pelvis"));
const FName NAME_root(TEXT("root"));
const FName NAME_spine_03(TEXT("spine_03"));

//#include "Character/ALSCharacter.h"
//
//#include "Engine/StaticMesh.h"
//#include "AI/ALSAIController.h"
//#include "Engine/World.h"
//#include "Kismet/GameplayStatics.h"

ASC_CharacterBase::ASC_CharacterBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	HeldObjectRoot = CreateDefaultSubobject<USceneComponent>(TEXT("HeldObjectRoot"));
	HeldObjectRoot->SetupAttachment(GetMesh());

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetupAttachment(HeldObjectRoot);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(HeldObjectRoot);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
}

void ASC_CharacterBase::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

}

void ASC_CharacterBase::ClearHeldObject()
{
	StaticMesh->SetStaticMesh(nullptr);
	SkeletalMesh->SetSkeletalMesh(nullptr);
	SkeletalMesh->SetAnimInstanceClass(nullptr);
}

void ASC_CharacterBase::AttachToHand(UStaticMesh* NewStaticMesh, USkeletalMesh* NewSkeletalMesh, UClass* NewAnimClass,
	bool bLeftHand, FVector Offset)
{
	ClearHeldObject();

	if (IsValid(NewStaticMesh))
	{
		StaticMesh->SetStaticMesh(NewStaticMesh);
	}
	else if (IsValid(NewSkeletalMesh))
	{
		SkeletalMesh->SetSkeletalMesh(NewSkeletalMesh);
		if (IsValid(NewAnimClass))
		{
			SkeletalMesh->SetAnimInstanceClass(NewAnimClass);
		}
	}

	FName AttachBone;
	if (bLeftHand)
	{
		AttachBone = TEXT("VB LHS_ik_hand_gun");
	}
	else
	{
		AttachBone = TEXT("VB RHS_ik_hand_gun");
	}

	HeldObjectRoot->AttachToComponent(GetMesh(),
		FAttachmentTransformRules::SnapToTargetNotIncludingScale, AttachBone);
	HeldObjectRoot->SetRelativeLocation(Offset);
}


void ASC_CharacterBase::AttachToHandActor_Implementation(TSubclassOf<AActor> NewActorClass, bool bLeftHand, FVector Offset, FRotator ROffset)
{
	ClearHeldObject();

	AActor* NewAct = nullptr;

	FActorSpawnParameters SParams;
	SParams.Owner = this;
	SParams.Instigator = this;

	FVector SVector;
	FRotator SRotator;

	if (HasAuthority())
	{
		NewAct = GetWorld()->SpawnActor(NewActorClass, &SVector, &SRotator, SParams);
	}

	if (!NewAct) return;

	NewAct->SetOwner(this);
	CurrentWeapon = Cast<ASC_WeaponBase>(NewAct);
	CurrentWeapon->WeaponOwner = this;

	FName AttachBone;
	if (bLeftHand)
	{
		AttachBone = TEXT("VB LHS_ik_hand_gun");
	}
	else
	{
		AttachBone = TEXT("VB RHS_ik_hand_gun");
	}

	NewAct->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, AttachBone);
	NewAct->SetActorRelativeLocation(Offset);
	NewAct->SetActorRelativeRotation(ROffset);
}

void ASC_CharacterBase::RagdollUpdate(float DeltaTime)
{
	// Set the Last Ragdoll Velocity.
	const FVector NewRagdollVel = GetMesh()->GetPhysicsLinearVelocity(NAME_root);
	LastRagdollVelocity = (NewRagdollVel != FVector::ZeroVector)
		? NewRagdollVel
		: LastRagdollVelocity / 2;

	// Use the Ragdoll Velocity to scale the ragdoll's joint strength for physical animation.
	const float SpringValue = FMath::GetMappedRangeValueClamped({ 0.0f, 1000.0f }, { 0.0f, 25000.0f },
		LastRagdollVelocity.Size());
	GetMesh()->SetAllMotorsAngularDriveParams(SpringValue, 0.0f, 0.0f, false);

	// Disable Gravity if falling faster than -4000 to prevent continual acceleration.
	// This also prevents the ragdoll from going through the floor.
	const bool bEnableGrav = LastRagdollVelocity.Z > -4000.0f;
	GetMesh()->SetEnableGravity(bEnableGrav);

	// Update the Actor location to follow the ragdoll.
	SetActorLocationDuringRagdoll(DeltaTime);
}

void ASC_CharacterBase::SetActorLocationDuringRagdoll(float DeltaTime)
{
	//if (IsLocallyControlled())
	//{
		// Set the pelvis as the target location.
		TargetRagdollLocation = GetMesh()->GetSocketLocation(NAME_Pelvis);
		if (!HasAuthority())
		{
			Server_SetMeshLocationDuringRagdoll(TargetRagdollLocation);
		}
	//}
}

void ASC_CharacterBase::RagdollStart()
{
	//if (RagdollStateChangedDelegate.IsBound())
	//{
	//	RagdollStateChangedDelegate.Broadcast(true);
	//}

	///** When Networked, disables replicate movement reset TargetRagdollLocation and ServerRagdollPull variable
	//and if the host is a dedicated server, change character mesh optimisation option to avoid z-location bug*/
	//MyCharacterMovementComponent->bIgnoreClientMovementErrorChecksAndCorrection = 1;

	//if (UKismetSystemLibrary::IsDedicatedServer(GetWorld()))
	//{
	//	DefVisBasedTickOp = GetMesh()->VisibilityBasedAnimTickOption;
	//	GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	//}

	//TargetRagdollLocation = GetMesh()->GetSocketLocation(NAME_root);
	//ServerRagdollPull = 0;

	//// Step 1: Clear the Character Movement Mode and set the Movement State to Ragdoll
	//GetCharacterMovement()->SetMovementMode(MOVE_None);
	//SetMovementState(EALSMovementState::Ragdoll);

	//// Step 2: Disable capsule collision and enable mesh physics simulation starting from the pelvis.
	//GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	//GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
	//GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	//GetMesh()->SetSimulatePhysics(true);
	//GetMesh()->SetAllBodiesBelowSimulatePhysics(NAME_root, true, true);

	//// Step 3: Stop any active montages.
	//MainAnimInstance->Montage_Stop(0.2f);

	//SetReplicateMovement(false);

	Super::RagdollStart();

	ClearHeldObject();

}

void ASC_CharacterBase::RagdollEnd()
{
	Super::RagdollEnd();
	UpdateHeldObject();
}

ECollisionChannel ASC_CharacterBase::GetThirdPersonTraceParams(FVector& TraceOrigin, float& TraceRadius)
{
	const FName CameraSocketName = bRightShoulder ? TEXT("TP_CameraTrace_R") : TEXT("TP_CameraTrace_L");
	TraceOrigin = GetMesh()->GetSocketLocation(CameraSocketName);
	TraceRadius = 15.0f;
	return ECC_Camera;
}

FTransform ASC_CharacterBase::GetThirdPersonPivotTarget()
{
	return FTransform(GetActorRotation(),
		(GetMesh()->GetSocketLocation(TEXT("Head")) + GetMesh()->GetSocketLocation(TEXT("root"))) / 2.0f,
		FVector::OneVector);
}

FVector ASC_CharacterBase::GetFirstPersonCameraTarget()
{
	return GetMesh()->GetSocketLocation(TEXT("FP_Camera"));
}

void ASC_CharacterBase::WeaponTryFireClient()
{
	if (CurrentWeapon)
	{
		FVector FirePoint = CurrentWeapon->SKM_Component->GetSocketLocation(CurrentWeapon->BulletSocket);
		FRotator FireDirection = CurrentWeapon->SKM_Component->GetSocketRotation(CurrentWeapon->BulletSocket);

		if (CurrentWeapon->CanWeaponFire() && IsLocallyControlled())
		{
			WeaponTryFire(FirePoint, FireDirection);
			CurrentWeapon->PlayFireEffect();
			CurrentWeapon->LastShootTime = GetWorld()->GetUnpausedTimeSeconds();

			AddControllerYawInput(FMath::FRandRange(CurrentWeapon->RecoilMinYaw, CurrentWeapon->RecoilMaxYaw));
			AddControllerPitchInput(FMath::FRandRange(CurrentWeapon->RecoilMinPitch, CurrentWeapon->RecoilMaxPitch));
		}

	}
}

void ASC_CharacterBase::WeaponTryFire_Implementation(FVector BulletPos, FRotator BulletRot)
{
	if (HasAuthority())
		CurrentWeapon->FireBullet(BulletPos, BulletRot);
}

void ASC_CharacterBase::JumpPressedAction()
{
	// return if not alive
	if (HealthComponent->GetLifeState() != ELifeState::Alive) return;
	
	Super::JumpPressedAction();
}

void ASC_CharacterBase::JumpReleasedAction()
{
	// return if not alive
	if (HealthComponent->GetLifeState() != ELifeState::Alive) return;

	Super::JumpReleasedAction();
}

void ASC_CharacterBase::ApplyRagdollImpulse_Implementation(FVector Impulse, FName BoneName)
{
	//GEngine->AddOnScreenDebugMessage(5, 5, FColor::Red, "before");
	RagdollStart();
	GetMesh()->AddImpulse(Impulse, BoneName);

	/*GetMesh()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);*/
}

void ASC_CharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward/Backwards", this, &ASC_CharacterBase::PlayerForwardMovementInput);
	PlayerInputComponent->BindAxis("MoveRight/Left", this, &ASC_CharacterBase::PlayerRightMovementInput);
	PlayerInputComponent->BindAxis("LookUp/Down", this, &ASC_CharacterBase::PlayerCameraUpInput);
	PlayerInputComponent->BindAxis("LookLeft/Right", this, &ASC_CharacterBase::PlayerCameraRightInput);
	PlayerInputComponent->BindAction("JumpAction", IE_Pressed, this, &ASC_CharacterBase::JumpPressedAction);
	PlayerInputComponent->BindAction("JumpAction", IE_Released, this, &ASC_CharacterBase::JumpReleasedAction);
	PlayerInputComponent->BindAction("StanceAction", IE_Pressed, this, &ASC_CharacterBase::StancePressedAction);
	PlayerInputComponent->BindAction("WalkAction", IE_Pressed, this, &ASC_CharacterBase::WalkPressedAction);
	//PlayerInputComponent->BindAction("RagdollAction", IE_Pressed, this, &ASC_CharacterBase::RagdollPressedAction);
	/*PlayerInputComponent->BindAction("SelectRotationMode_1", IE_Pressed, this, &AALSBaseCharacter::VelocityDirectionPressedAction);
	PlayerInputComponent->BindAction("SelectRotationMode_2", IE_Pressed, this, &AALSBaseCharacter::LookingDirectionPressedAction);*/
	PlayerInputComponent->BindAction("SprintAction", IE_Pressed, this, &ASC_CharacterBase::SprintPressedAction);
	PlayerInputComponent->BindAction("SprintAction", IE_Released, this, &ASC_CharacterBase::SprintReleasedAction);
	PlayerInputComponent->BindAction("AimAction", IE_Pressed, this, &ASC_CharacterBase::AimPressedAction);
	PlayerInputComponent->BindAction("AimAction", IE_Released, this, &ASC_CharacterBase::AimReleasedAction);
	//PlayerInputComponent->BindAction("CameraAction", IE_Pressed, this, &ASC_CharacterBase::CameraPressedAction);
	//PlayerInputComponent->BindAction("CameraAction", IE_Released, this, &ASC_CharacterBase::CameraReleasedAction);
	PlayerInputComponent->BindAction("FireAction", IE_Pressed, this, &ASC_CharacterBase::FireActionOpen);
	PlayerInputComponent->BindAction("FireAction", IE_Released, this, &ASC_CharacterBase::FireActionClose);
}

void ASC_CharacterBase::OnOverlayStateChanged(EALSOverlayState PreviousState)
{
	Super::OnOverlayStateChanged(PreviousState);
	UpdateHeldObject();
}

void ASC_CharacterBase::OnPlayerRespawn()
{

}

void ASC_CharacterBase::ReceiveBulletHit_Implementation(FName BulletName, FName HitBone, FVector BulletVelocity)
{
	FBulletDT BulletSettings;
	bool bValidData = false;

	USC_GameInstance* GI = Cast<USC_GameInstance>(GetWorld()->GetGameInstance());
	
	if (GI)
		bValidData = GI->GetBulletInfo(BulletName, BulletSettings);

	if (!bValidData) return;

	// Get bullet speed and calculate actual bullet damage, because it depends of bullet speed
	float DamageMultiplier = 0.f;
	float BulletCurrentSpeed = UKismetMathLibrary::VSize(BulletVelocity);
	float BulletActualDamage = UKismetMathLibrary::MapRangeClamped(BulletCurrentSpeed, 0, BulletSettings.BulletMaxSpeed, 0, BulletSettings.BulletMaxDamage);

	if (HealthComponent)
	{
		HealthComponent->AddDamageToPart(BulletActualDamage, HitBone);

		if (HealthComponent->GetLifeState() == ELifeState::Knocked || HealthComponent->GetLifeState() == ELifeState::Dead)
		{
			UKismetMathLibrary::Vector_Normalize(BulletVelocity);
			ApplyRagdollImpulse(BulletVelocity * 500, HitBone);
		}
	}
}

void ASC_CharacterBase::CollectParentHitPartNames(TMap<FName, float>& OutTMap)
{
	// Copy TMap structure of parts
	OutTMap = BoneDamageMultipliers;

	// Init all hit parts with 0 damage
	for (TPair<FName, float> el : OutTMap)
		el.Value = 0;
}

float ASC_CharacterBase::GetHitPartArmor(FName PartName)
{
	bool bValidPart = BoneDamageMultipliers.Contains(PartName);
	if (!bValidPart) return 1.f;

	float PartArmor = BoneDamageMultipliers[PartName];

	return BoneDamageMultipliers[PartName];
}

void ASC_CharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateHeldObjectAnimations();

	if (bFireActionPressed && HealthComponent->GetLifeState() == ELifeState::Alive)
	{
		WeaponTryFireClient();
	}
}

void ASC_CharacterBase::BeginPlay()
{
	Super::BeginPlay();

	UpdateHeldObject();
}

void ASC_CharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASC_CharacterBase, CurrentWeapon, COND_None);
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_WeaponBase.h"
#include "SC_BulletBase.h"
#include "SC_CharacterBase.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

class ASC_CharacterBase;

ASC_WeaponBase::ASC_WeaponBase(const FObjectInitializer& ObjectInitializer)
{
	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->SetupAttachment(RootComponent);
	CollisionBox->InitBoxExtent(FVector(10.f, 10.f, 10.f));

	SM_Component->SetupAttachment(CollisionBox);
	SKM_Component->SetupAttachment(CollisionBox);

	FireFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FireFX"));
	FireFX->SetupAttachment(SKM_Component, BulletSocket);
	FireFX->bAutoActivate = false;

	bReplicates = true;


}

void ASC_WeaponBase::PlayFireEffect()
{
	if (!CanWeaponFire()) return;

	if (FireFX) FireFX->Activate(true);
	if (SoundFire) UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundFire, this->GetActorLocation());
}

void ASC_WeaponBase::FireBullet_Implementation(FVector BulletSpawnLoc, FRotator BulletSpawnRot)
{
	//if (!CanWeaponFire()) return; //moved to client side because of network latency bullshit

	// if [WeaponOwner] is nullptr that means we are not weapon owners, so can call FX (execute all except owner, because owner already executed it locally)
	if (!WeaponOwner) PlayFireEffect();

	LastShootTime = GetWorld()->GetUnpausedTimeSeconds();

	if (HasAuthority())
	{
		FActorSpawnParameters SParams;
		SParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		AActor* Bullet = GetWorld()->SpawnActor(BulletClass, &BulletSpawnLoc, &BulletSpawnRot);

		// Manual activate FX on host or dedic
		if (FireFX) FireFX->Activate(true);
		if (SoundFire) UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundFire, this->GetActorLocation());
	}
}

bool ASC_WeaponBase::CanWeaponFire()
{
	float time = GetWorld()->GetUnpausedTimeSeconds();
	return time > LastShootTime + WeaponFireInterval;
}

void ASC_WeaponBase::BeginPlay()
{
	Super::BeginPlay();

	USC_GameInstance* GI = Cast<USC_GameInstance>(GetWorld()->GetGameInstance());
	bool bValidData = GI->GetWeaponInfo(WeaponName, WeaponSettings);

	if (bValidData)
	{
		WeaponFireInterval = WeaponSettings.WeaponFireInterval;
		SoundFire = WeaponSettings.SoundFire;
		BulletClass = WeaponSettings.BulletClass;
		BulletSocket = WeaponSettings.BulletSocket;
		BulletSilSocket = WeaponSettings.BulletSilSocket;
		
		RecoilMinYaw = WeaponSettings.RecoilMinYaw;
		RecoilMaxYaw = WeaponSettings.RecoilMaxYaw;
		RecoilMinPitch = WeaponSettings.RecoilMinPitch;
		RecoilMaxPitch = WeaponSettings.RecoilMaxPitch;

		FireFX->SetTemplate(WeaponSettings.FireFX);

		UE_LOG(LogTemp, Warning, TEXT("Weapon init DT done"));
	}

}

void ASC_WeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

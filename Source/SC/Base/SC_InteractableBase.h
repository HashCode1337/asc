// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SC_InteractableBase.generated.h"

UCLASS()
class SC_API ASC_InteractableBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASC_InteractableBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Component", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* SM_Component = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Component", meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* SKM_Component = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

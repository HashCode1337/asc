// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "../Libraries/SC_Statics.h"
#include "Engine/DataTable.h"
#include "SC_BulletBase.generated.h"

class UProjectileMovementComponent;
class USphereComponent;
class UParticleSystemComponent;

UCLASS(Blueprintable)
class SC_API ASC_BulletBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASC_BulletBase();

	void OnConstruction(const FTransform& Transform) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Bullet Settings")
	FName BulletName = TEXT("BulletBase");

	UPROPERTY()
	FBulletDT BulletSettings;

	UPROPERTY()
	float LastBulletHitTime = 0.f;
	UPROPERTY()
	float LastBulletHitTimeThreshold = 0.1f;
	UPROPERTY()
	int32 RicochetsCount = 0.1f;
	UPROPERTY()
	int32 RicochetsCountMax = 0.1f;



	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Bullet Settings")
	class USphereComponent* CollSphere = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Bullet Settings")
	class UParticleSystemComponent* PSComp = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Bullet Settings")
	class UProjectileMovementComponent* PMVComp = nullptr;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnBulletHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

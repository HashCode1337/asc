// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SC_InteractableBase.h"
#include "Components/BoxComponent.h"
#include "../Libraries/SC_Statics.h"
#include "../Gameplay/SC_GameInstance.h"
#include "SC_WeaponBase.generated.h"


class ASC_BulletBase;
class ASC_CharacterBase;
/**
 * 
 */
UCLASS(Blueprintable)
class SC_API ASC_WeaponBase : public ASC_InteractableBase
{
	GENERATED_BODY()

public:

	ASC_WeaponBase(const FObjectInitializer& ObjectInitializer);
	
	// Core
	/*------------------------------------------------------------------------------------------------------------------------*/



	UPROPERTY()
	float LastShootTime = -1.f;
	
	UPROPERTY()
	ASC_CharacterBase* WeaponOwner;



	// Exposed Properties
	/*------------------------------------------------------------------------------------------------------------------------*/



	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Weapon Settings")
	FName WeaponName = TEXT("Wpn_WeaponBase");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Weapon Settings")
	UBoxComponent* CollisionBox = nullptr;




	// Properties which will be taken from WeaponDataTable
	/*------------------------------------------------------------------------------------------------------------------------*/
	


	UPROPERTY()
	UParticleSystemComponent* FireFX;
	UPROPERTY()
	FWeaponDT WeaponSettings;
	/*how frequently weapon can fires*/
	UPROPERTY()
	float WeaponFireInterval = 0.5;
	UPROPERTY()
	USoundBase* SoundFire = nullptr;
	UPROPERTY()
	TSubclassOf<ASC_BulletBase> BulletClass;
	/*Where bullet will be spawned if weapon has not silencer*/
	UPROPERTY()
	FName BulletSocket = TEXT("Bullet");
	/*Where bullet will be spawned if weapon has silencer*/
	UPROPERTY()
	FName BulletSilSocket = TEXT("BulletSil");
	UPROPERTY()
	float RecoilMinYaw = -0.2f;
	UPROPERTY()
	float RecoilMaxYaw = 0.2f;
	UPROPERTY()
	float RecoilMinPitch = -0.2f;
	UPROPERTY()
	float RecoilMaxPitch = -0.4f;




	/*------------------------------------------------------------------------------------------------------------------------*/

	UFUNCTION(BlueprintCallable)
	void PlayFireEffect();

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void FireBullet(FVector BulletSpawnLoc, FRotator BulletSpawnRot);

	UFUNCTION(BlueprintCallable)
	bool CanWeaponFire();

	/*------------------------------------------------------------------------------------------------------------------------*/



protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	
};

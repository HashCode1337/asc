// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/ALSBaseCharacter.h"
#include "../Interfaces/Damage.h"
#include "../Components/HealthComponent.h"
#include "SC_CharacterBase.generated.h"

/**
 *
 */
class ASC_WeaponBase;


UCLASS(Blueprintable, BlueprintType)
class SC_API ASC_CharacterBase : public AALSBaseCharacter, public IDamage
{
	GENERATED_BODY()

public:
	ASC_CharacterBase(const FObjectInitializer& ObjectInitializer);
	void OnConstruction(const FTransform& Transform);

	UPROPERTY(EditDefaultsOnly)
	EFaction Faction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SC|Extra")
	TMap<FName, float> BoneDamageMultipliers = { 
		{TEXT("root"), 1.f},
		{TEXT("ik_foot_root"), 1.f},
		{TEXT("ik_foot_l"), 1.f},
		{TEXT("ik_foot_r"), 1.f},
		{TEXT("ik_hand_root"), 1.f},
		{TEXT("ik_hand_gun"), 1.f},
		{TEXT("ik_hand_l"), 1.f},
		{TEXT("ik_hand_r"), 1.f},
		{TEXT("pelvis"), 1.f},
		{TEXT("spine_01"), 1.f},
		{TEXT("spine_02"), 1.f},
		{TEXT("spine_03"), 1.f},
		{TEXT("clavicle_l"), 1.f},
		{TEXT("upperarm_l"), 1.f},
		{TEXT("lowerarm_l"), 1.f},
		{TEXT("hand_l"), 1.f},
		{TEXT("index_01_l"), 1.f},
		{TEXT("index_02_l"), 1.f},
		{TEXT("index_03_l"), 1.f},
		{TEXT("middle_01_l"), 1.f},
		{TEXT("middle_02_l"), 1.f},
		{TEXT("middle_03_l"), 1.f},
		{TEXT("pinky_01_l"), 1.f},
		{TEXT("pinky_02_l"), 1.f},
		{TEXT("pinky_03_l"), 1.f},
		{TEXT("ring_01_l"), 1.f},
		{TEXT("ring_02_l"), 1.f},
		{TEXT("ring_03_l"), 1.f},
		{TEXT("thumb_01_l"), 1.f},
		{TEXT("thumb_02_l"), 1.f},
		{TEXT("thumb_03_l"), 1.f},
		{TEXT("lowerarm_twist_01_l"), 1.f},
		{TEXT("upperarm_twist_01_l"), 1.f},
		{TEXT("clavicle_r"), 1.f},
		{TEXT("upperarm_r"), 1.f},
		{TEXT("lowerarm_r"), 1.f},
		{TEXT("hand_r"), 1.f},
		{TEXT("index_01_r"), 1.f},
		{TEXT("index_02_r"), 1.f},
		{TEXT("index_03_r"), 1.f},
		{TEXT("middle_01_r"), 1.f},
		{TEXT("middle_02_r"), 1.f},
		{TEXT("middle_03_r"), 1.f},
		{TEXT("pinky_01_r"), 1.f},
		{TEXT("pinky_02_r"), 1.f},
		{TEXT("pinky_03_r"), 1.f},
		{TEXT("ring_01_r"), 1.f},
		{TEXT("ring_02_r"), 1.f},
		{TEXT("ring_03_r"), 1.f},
		{TEXT("thumb_01_r"), 1.f},
		{TEXT("thumb_02_r"), 1.f},
		{TEXT("thumb_03_r"), 1.f},
		{TEXT("lowerarm_twist_01_r"), 1.f},
		{TEXT("upperarm_twist_01_r"), 1.f},
		{TEXT("neck_01"), 1.f},
		{TEXT("head"), 5.f},
		{TEXT("thigh_l"), 1.f},
		{TEXT("calf_l"), 1.f},
		{TEXT("calf_twist_01_l"), 1.f},
		{TEXT("foot_l"), 1.f},
		{TEXT("ball_l"), 1.f},
		{TEXT("thigh_twist_01_l"), 1.f},
		{TEXT("thigh_r"), 1.f},
		{TEXT("calf_r"), 1.f},
		{TEXT("calf_twist_01_r"), 1.f},
		{TEXT("foot_r"), 1.f},
		{TEXT("ball_r"), 1.f},
		{TEXT("thigh_twist_01_r"), 1.f}
	};



	/** Implemented on BP to update held objects and overrides */
	/*------------------------------------------------------------------------------------------------------------------------*/
	
	
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "ALS|HeldObject")
	void UpdateHeldObject();

	UFUNCTION(BlueprintCallable, Category = "ALS|HeldObject")
	void ClearHeldObject();

	UFUNCTION(BlueprintCallable, Category = "ALS|HeldObject")
	void AttachToHand(UStaticMesh* NewStaticMesh, USkeletalMesh* NewSkeletalMesh,
	class UClass* NewAnimClass, bool bLeftHand, FVector Offset);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "ALS|HeldObject")
	void AttachToHandActor(TSubclassOf<AActor> NewActorClass, bool bLeftHand, FVector Offset, FRotator ROffset);

	virtual void RagdollUpdate(float DeltaTime);
	virtual void SetActorLocationDuringRagdoll(float DeltaTime) override;
	virtual void RagdollStart() override;
	virtual void RagdollEnd() override;
	virtual ECollisionChannel GetThirdPersonTraceParams(FVector& TraceOrigin, float& TraceRadius) override;
	virtual FTransform GetThirdPersonPivotTarget() override;
	virtual FVector GetFirstPersonCameraTarget() override;



	// Weapon
	/*------------------------------------------------------------------------------------------------------------------------*/
	


	UPROPERTY(Replicated, VisibleDefaultsOnly, BlueprintReadWrite, Category = "SC|Weapon");
	ASC_WeaponBase* CurrentWeapon = nullptr;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "SC|Weapon");
	bool bFireActionPressed = false;

	UFUNCTION(BlueprintCallable)
	void FireActionOpen() {bFireActionPressed = true;};

	UFUNCTION(BlueprintCallable)
	void FireActionClose() {bFireActionPressed = false;};

	UFUNCTION(BlueprintCallable, Category = "SC|Weapon")
	void WeaponTryFireClient();

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "SC|Weapon")
	void WeaponTryFire(FVector BulletPos, FRotator BulletRot);



	// Input
	/*------------------------------------------------------------------------------------------------------------------------*/
	

	
	void JumpPressedAction() override;
	void JumpReleasedAction() override;


	
	// Damage and Health
	/*------------------------------------------------------------------------------------------------------------------------*/
	


	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "SC|Damage")
	void ApplyRagdollImpulse(FVector Impulse, FName BoneName);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ALS|Damage")
	UHealthComponent* HealthComponent = nullptr;

	// Input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual void OnPlayerRespawn();

protected:
	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;
	virtual void OnOverlayStateChanged(EALSOverlayState PreviousState) override;

	

	// SC Interfaces
	/*------------------------------------------------------------------------------------------------------------------------*/



	UFUNCTION(Server, Reliable, BlueprintCallable)
	virtual void ReceiveBulletHit(FName BulletName, FName HitBone, FVector BulletVelocity);
	
	/*Filling up bone names while construction object*/
	UFUNCTION(BlueprintCallable)
	virtual void CollectParentHitPartNames(TMap<FName, float>& OutTMap);	
	
	/*Get body part armor multiplier*/
	UFUNCTION(BlueprintCallable)
	virtual float GetHitPartArmor(FName PartName);



	/** Implement on BP to update animation states of held objects */
	/*------------------------------------------------------------------------------------------------------------------------*/
	


	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "ALS|HeldObject")
	void UpdateHeldObjectAnimations();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ALS|Component")
	USceneComponent* HeldObjectRoot = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ALS|Component")
	USkeletalMeshComponent* SkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ALS|Component")
	UStaticMeshComponent* StaticMesh = nullptr;

private:
	bool bNeedsColorReset = false;
};
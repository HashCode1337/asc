// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_InteractableBase.h"

// Sets default values
ASC_InteractableBase::ASC_InteractableBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootHolder"));

	SM_Component = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SM_Component"));
	SM_Component->SetupAttachment(RootComponent);

	SKM_Component = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SKM_Component"));
	SKM_Component->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASC_InteractableBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASC_InteractableBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


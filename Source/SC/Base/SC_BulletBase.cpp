// Fill out your copyright notice in the Description page of Project Settings.


#include "SC_BulletBase.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "../Gameplay/SC_GameInstance.h"
#include "../Gameplay/SC_GameState.h"
#include "SC_CharacterBase.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
ASC_BulletBase::ASC_BulletBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	RootComponent = CollSphere;
	CollSphere->bReturnMaterialOnMove = true;
	CollSphere->SetCollisionProfileName(TEXT("Bullet"));
	CollSphere->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);

	CollSphere->OnComponentHit.AddDynamic(this, &ASC_BulletBase::OnBulletHit);

	PSComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Tracer"));
	PSComp->SetupAttachment(RootComponent);

	PMVComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjMov"));
	PMVComp->UpdatedComponent = RootComponent;
	PMVComp->bShouldBounce = true;
	PMVComp->bRotationFollowsVelocity = true;

	bReplicates = true;

}

void ASC_BulletBase::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	
}

// Called when the game starts or when spawned
void ASC_BulletBase::BeginPlay()
{
	Super::BeginPlay();

	USC_GameInstance* GI = Cast<USC_GameInstance>(GetWorld()->GetGameInstance());
	bool bValidData = GI->GetBulletInfo(BulletName, BulletSettings);
	SetLifeSpan(BulletSettings.BulletLifetime);
	PMVComp->SetVelocityInLocalSpace(FVector(BulletSettings.BulletMaxSpeed, 0, 0));

	if (!HasAuthority()) return;

	if (GI)
	{
		if (bValidData)
		{
			// Init bullet params
			PMVComp->MaxSpeed = BulletSettings.BulletMaxSpeed;
			PMVComp->InitialSpeed = BulletSettings.BulletMaxSpeed;
			RicochetsCountMax = BulletSettings.BulletMaxRicochets;


			// Ricochet probability
			float RicochetProb = FMath::RandRange(0.f, 1.f);

			if (BulletSettings.BulletRicochetProbability >= RicochetProb)
				PMVComp->bShouldBounce = true;
			else
				PMVComp->bShouldBounce = false;
		}
	}
	
}

void ASC_BulletBase::OnBulletHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!OtherActor) return;

	EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(Hit);
	FTransform ImpactData = FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.f));
	
	//Don't get velocity from actor, because it has late result bug and often returns 0 
	FVector_NetQuantizeNormal NewImpulse = PMVComp->Velocity;
	

	if (HasAuthority())
	{
		
		if (RicochetsCount >= RicochetsCountMax)
			PMVComp->bShouldBounce = false;
		
		RicochetsCount++;

		// Get GS because PlayBulletImpactFX() is there
		ASC_GameState* GS = (ASC_GameState*)UGameplayStatics::GetGameState(GetWorld());
		
		// Fix threshold time to prevent sound spaming
		float CurrentTime = UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld());
		if (CurrentTime > LastBulletHitTime + LastBulletHitTimeThreshold)
		{
			LastBulletHitTime = CurrentTime;
			GS->PlayBulletImpactFX(BulletName, SurfaceType, ImpactData, true);
		}
		else
		{
			GS->PlayBulletImpactFX(BulletName, SurfaceType, ImpactData, false);
		}

		// Damage calculation
		IDamage* Victim = Cast<IDamage>(OtherActor);
		
		if (Victim)
			Victim->ReceiveBulletHit(BulletName, Hit.BoneName, NewImpulse);
	}
	
	// Hide bullet tracer, because we don't give a fuck about it after hit
	SetActorHiddenInGame(true);
}

// Called every frame
void ASC_BulletBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


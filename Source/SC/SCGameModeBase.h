// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SCGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SC_API ASCGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void RespawnPlayer(APawn* Player, APlayerController* PC);
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Chaos/ChaosEngineInterface.h"
#include "Particles/ParticleSystem.h"
#include "Engine/DataTable.h"
#include "SC_Statics.generated.h"

class UBoxComponent;
class ASC_BulletBase;

UENUM(BlueprintType)
enum class EInteractableType : uint8
{
	Unknown UMETA(DisplayName = "Unknown"),
	Pickable UMETA(DisplayName = "Pickable")
};

UENUM(BlueprintType)
enum class EFaction : uint8
{
	Loner UMETA(DisplayName = "Loner"),
	Neutral UMETA(DisplayName = "Neutral"),
	Bandit UMETA(DisplayName = "Bandit"),
	Freedom UMETA(DisplayName = "Freedom"),
	Dolg UMETA(DisplayName = "Dolg"),
	Merc UMETA(DisplayName = "Merc"),
	Monolith UMETA(DisplayName = "Monolith")
};

UENUM(BlueprintType)
enum class ELifeState : uint8
{
	Alive UMETA(DisplayName = "Alive"),
	Knocked UMETA(DisplayName = "Knocked"),
	Dead UMETA(DisplayName = "Dead")
};

USTRUCT(BlueprintType)
struct FBulletDT : public FTableRowBase
{
	GENERATED_BODY()
public:

	// Bullet Lifetime
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BulletLifetime = 1.f;

	// Physics
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BulletMaxSpeed = 10000.f;

	/*Max damage will be applied if max speed reached
	100% max speed = 100% max damage*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BulletMaxDamage = 100.f;

	/*probability of bullet will bounce (0 min - 1 max)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0.f, ClampMax = 1.f))
	float BulletRicochetProbability = 0.05f;

	/*how many times bullet could ricochets*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0, ClampMax = 1))
	int BulletMaxRicochets = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSounds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitParticles;

};

USTRUCT(BlueprintType)
struct FWeaponDT : public FTableRowBase
{
	GENERATED_BODY()
public:

	/*how frequently weapon can fires*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float WeaponFireInterval = 0.5;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USoundBase* SoundFire = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ASC_BulletBase> BulletClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UParticleSystem* FireFX;

	/*Where bullet will be spawned if weapon has not silencer*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName BulletSocket = TEXT("Bullet");

	/*Where bullet will be spawned if weapon has silencer*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName BulletSilSocket = TEXT("BulletSil");

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float RecoilMinYaw = -0.2f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float RecoilMaxYaw = 0.2f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float RecoilMinPitch = -0.2f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float RecoilMaxPitch = -0.4f;
};

UCLASS()
class SC_API USC_Statics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	//UFUNCTION(BlueprintCallable)
	//static bool GetBulletInfo(FName BulletName, UDataTable* BulletDT, FBulletDT& BulletData);

};

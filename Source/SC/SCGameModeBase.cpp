// Copyright Epic Games, Inc. All Rights Reserved.


#include "SCGameModeBase.h"
#include "Gameplay/SC_GameInstance.h"
#include "Libraries/SC_Statics.h"
#include "Base/SC_CharacterBase.h"
#include "Kismet/GameplayStatics.h"
#include "SC_HUD.h"
#include "Points/SC_RespawnPointFaction.h"


void ASCGameModeBase::RespawnPlayer_Implementation(APawn* Player, APlayerController* PC)
{
	if (!Player || !PC) return;

	PC->UnPossess();

	UE_LOG(LogTemp, Warning, TEXT("Pawn %s, PC %s"), *Player->GetName(), *PC->GetName());

	EFaction PlayerFaction;
	TArray<AActor*> RP;

	FVector SpawnLocation(0.f, 0.f, 0.f);
	FRotator SpawnRotation(0.f, 0.f, 0.f);

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASC_RespawnPointFaction::StaticClass(), RP);

	ASC_CharacterBase* DeadCharacter = (ASC_CharacterBase*)(Player);
	if (DeadCharacter)
	{
		PlayerFaction = DeadCharacter->Faction;

		for (AActor* Point : RP)
		{
			ASC_RespawnPointFaction* RPPoint = (ASC_RespawnPointFaction*)(Point);
			if (RPPoint)
			{
				bool bIsFactionMatch = RPPoint->RespawnPointFaction == PlayerFaction;
				if (bIsFactionMatch)
				{
					SpawnLocation = RPPoint->GetActorLocation();
					SpawnRotation = RPPoint->GetActorRotation();
					break;
				}
			}
		}

		ASC_CharacterBase* NewPlayer = (ASC_CharacterBase*)(GetWorld()->SpawnActor(Player->GetClass(), &SpawnLocation, &SpawnRotation));

		if (!NewPlayer)
		{
			UE_LOG(LogTemp, Error, TEXT("ERROR CREATING NEW PLAYER %s %s"), *Player->GetName(), *PC->GetName());
			return;
		}

		PC->Possess(NewPlayer);
		//NewPlayer->OnPlayerRespawn();
		NewPlayer->Faction = PlayerFaction;
		

	}

}
